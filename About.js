import React, {useState, useEffect} from 'react'
import {
  SafeAreaView,
  StyleSheet,
  Image,
  View,
  Text,
  TextInput,
  Dimensions,
  ScrollView,
  ImageBackground,
  TouchableOpacity,
  ToastAndroid,
  KeyboardAvoidingView,
  FlatList,
} from 'react-native'
import {fontColor, fontFamily} from '../config/config'
import BottomComponent from '../components/BottomComponent'
import HeaderComponent from '../components/HeaderComponent'
import NavigationService from '../services/NavigationService'

const HEIGHT = Dimensions.get('window').height
const WIDTH = Dimensions.get('window').width

const About = ({props, route}) => {
  const [question, setQuestion] = useState('')
  const [updated, setUpdated] = useState(false)
  const [brandAdmins, setBrandAdmins] = useState([])
  const [loading, setLoading] = useState(true)

  return (
    <KeyboardAvoidingView
      style={{flex: 1}}
      behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
      keyboardVerticalOffset={Platform.OS === 'ios' ? 40 : -200}>
      <SafeAreaView style={{flex: 1}}>
        <View style={{flex: 1.5}}>
          <HeaderComponent
            title={'About App'}
            showSearch={false}
            showBackButtonAndTitle={true}
          />
        </View>

        <View style={{flex: 8}}>
          <View
            style={{
              height: HEIGHT - 250,
              marginTop: 20,
              width: '95%',
              alignSelf: 'center',
              backgroundColor: 'lightgray',
              borderRadius: 15,
            }}>
            <Text
              style={{
                alignSelf: 'center',
                marginBottom: 20,
                marginTop: 10,
                fontFamily: fontFamily.semiBold,
                fontSize: 16,
              }}>
              About
            </Text>
            <Text style={styles.text}>
              Lorem ipsum dolor sit amet. Et omnis impedit et suscipit maxime
              cum nemo quidem! Eos sunt repellat sed quaerat omnis et autem
              velit qui quia aspernatur qui amet enim qui quia distinctio ut
              veritatis officiis. Eum labore voluptatibus hic ipsa architecto
              non voluptas reprehenderit sed omnis facere rem ipsam quia? Ut
              libero beatae est velit placeat sed alias nulla eum voluptas optio
              eos quibusdam autem. Ad nostrum nihil vel blanditiis facilis ut
              laudantium fuga ea internos minima vel fugiat veritatis est
              mollitia voluptatem? Aut libero dicta At quaerat sunt ab
              perferendis voluptas 33 nostrum dolores sed quia fugit? Qui
              molestiae quaerat in mollitia libero eos excepturi ratione sed
              galisum vitae. Id nihil laudantium et recusandae assumenda non
              incidunt repudiandae qui eligendi quod rem mollitia porro aut
              internos debitis. Est ducimus blanditiis qui suscipit cumque et
              neque atque et consequatur earum et ratione fuga aut dignissimos
              odio est impedit eveniet.
            </Text>
          </View>
          {/* <Spinner
            visible={loading}
            textContent={'Loading...'}
            textStyle={styles.spinnerTextStyle}
          /> */}
        </View>
        <View style={{flex: 1.4}}>
          <BottomComponent />
        </View>
      </SafeAreaView>
    </KeyboardAvoidingView>
  )
}

const styles = StyleSheet.create({
  horAndVerCenter: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  container: {
    flex: 1,
    backgroundColor: 'red',
  },
  brand: {
    fontSize: 15,
    fontFamily: fontFamily.semiBold,
    color: fontColor.blue,
  },
  spinnerTextStyle: {
    color: '#FFF',
  },
  text: {
    padding: 15,
  },
})

export default About
